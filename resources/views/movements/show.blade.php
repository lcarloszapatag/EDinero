@extends('layouts.app')

@section('content')

    <h1>Detalles del movimiento {{ $movement->id }}</h1>

    <table class="table table-bordered">
        <tr>
            <th>Tipo</th>
            <td>{{ $movement->type }}</td>
        </tr>
        <tr>
            <th>Fecha</th>
            <td>{{ $movement->movement_date->format('d/m/Y') }}</td>
        </tr>
        <tr>
            <th>Categoría</th>
            <td>{{ $movement->category->name }}</td>
        </tr>
        <tr>
            <th>Cantidad</th>
            <td>{{ number_format($movement->money_decimal, 2) }}</td>
        </tr>
        <tr>
            <th>Descripción</th>
            <td>{{ $movement->description }}</td>
        </tr>
    </table>

    @if ($movement->image)
        <a href="{{ asset($movement->image) }}" class="thumbnail" target="_blank">
            <img src="{{ asset($movement->image) }}" alt="{{ $movement->id }}">
        </a>
    @endif

@endsection