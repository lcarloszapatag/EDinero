<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreMovement extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => [
                'required',
                Rule::in(['Egreso', 'Ingreso'])
            ],
            'movement_date' => 'required|date',
            'category_id' => 'required',
            'description' => 'required|min:3|max:1000',
            'money_decimal' => 'required|numeric|min:0.01',
            'image' => 'image'
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'El campo Tipo es requerido',
            'type.in' => 'El valor del campo tipo no es válido',
            'movement_date.required' => 'El campo Fecha es requerido',
            'movement_date.date' => 'La Fecha no es válida',
            'category_id.required' => 'La categoría es obligatoria',
            'description.required' => 'La descripción es obligatoria',
            'description.min' => 'La descripción debe tener tres caracteres o más',
            'description.max' => 'La descripción no puede tener más de 1000 caracteres',
            'money_decimal.required' => 'El monto es obligatorio',
            'money_decimal.numeric' => 'El monto debe ser un número',
            'money_decimal.min' => 'El monto debe ser mayor a cero',
            'image.image' => 'El archivo adjunto no es una imagen válida'
        ];
    }
}
