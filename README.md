# EDinero
Registro de entradas y salidas de dinero.

Crear el proyecto

```
laravel new EDinero
```

Crear las migraciones

```
php artisan make:migration create_categories_table --create="categories"
php artisan make:migration create_movements_table --create="movements"
```

Crear base de datos

Editar el archivo `env`

Ejecutar las migraciones

```
php artisan migrate
```

Usar la lógica Auth de Laravel

```
php artisan make:auth
```

Crear los modelos

```
php artisan make:model Category
php artisan make:model Movement
```

Agregar las propiedades:
  
```
protected $table = '';
protected $fillable = [];
protected $dates = [];
```
  
Crear la función `getMoneyDecimalAttribute()`


Crear el controlador

```
php artisan make:controller MovementsController --resource
```

Agregar la ruta resource a `web.php`

```
Route::resource('movements', 'MovementsController');
```

Instalar `laravelcollective/html

```
composer require "laravelcollective/html":"^5.4"
```

Agregar el service provider en `config/app.php`: 
  
```
Collective\Html\HtmlServiceProvider::class,
```
   
Agregar los aliases:
  
```
'Form' => Collective\Html\FormFacade::class,
'Html' => Collective\Html\HtmlFacade::class,
```

Crear la vista para registrar un movimiento

```
movements/partials/form.blade.php
movements/create.blade.php
```

Retornar la vista en MovementsController@create

Usar select2 para la categoría

Usar CDN:

```
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
```

Agregar @yield('scripts') al final del body en layouts/app.blade.php

Crear el Request StoreMovement

```
php artisan make:request StoreMovement
```

Autorizar sólo a usuarios logueados

Establecer reglas de validación

Asignar mensajes personalizados

Procesar el formulario en `MovementsController@store`

```
Crea un enlace simbólico "public/storage" a "storage/app/public" que sólo funciona en linux y MacOS
php artisan storage:link
```

ó

```
Configurar el Storage para usar directamente la carpeta public (public_path) en config/filesystems.php:
'default' => 'public',
'disks' => [
        //...
        'public' => [
            'driver' => 'local',
            'root' => public_path(),
            'url' => env('APP_URL').'/',
            'visibility' => 'public',
        ],
        //...
    ],
```

Crear la vista `movements/show.blade.php`

Mostrar todos los datos incluyendo la imagen.

Enlazar la imagen para abrirla sola en una pestaña nueva

Crear la vista `movements/index.blade.php`

Crear seeders para categorías y usuarios. `php artisan make:seeder CategoriesTableSeeder`

Mostrar la lista de movimientos registrados ordenados por fecha descendente (sin la imagen)

Colocar por cada fila los botones: editar y mostrar más detalles

```
npm install
npm run production
```

Crea la lógica de edición

Vista `movements/edit.blade.php`

MovementController@edit

MovementController@update
  