<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movements', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            // Ingreso ó Egreso
            $table->string('type', 50);

            $table->dateTime('movement_date');
            $table->unsignedInteger('category_id');
            $table->string('description', 1000);

            // Centavos para evitar el uso de decimales
            $table->unsignedInteger('money');

            // Ruta de la imagen
            $table->string('image')->nullable();

            $table->unsignedInteger('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements');
    }
}
